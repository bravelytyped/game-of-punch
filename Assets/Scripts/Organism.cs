using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Organism : MonoBehaviour
{
    public virtual int Health { get; protected set; }
    public ObjectPool parentPool;
    public virtual int NumOfOffspring { get; set; } = 1;
    // Start is called before the first frame update

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    protected void Die()
    {
        if (this.Health <= 0)
        {
            this.gameObject.SetActive(false);
            Debug.Log("I have perished");
        }
    }

    protected virtual void Reproduce(ObjectPool pool, Vector2 size, Vector2 location, int numToSpawn)
    {
        Spawner.sharedInstance.SpawnRequest(pool, size, location, numToSpawn);
    }
}
