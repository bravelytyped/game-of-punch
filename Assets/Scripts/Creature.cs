using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Collider2D))]
public class Creature : Organism
{
    //this is a parent class for all the living things in the game
    public int Hunger { get; set; }
    public int Thirst { get; set; }
    public int Mating { get; set; }
    public float Speed { get; set; }

    [SerializeField] private int health, hunger, thirst, mating;
    [SerializeField] private float speed = 1f;
    public Collider2D spawnCheck;
    public float fieldOfVision = 10f;

    public GameObject currentTarget;

    // Organisms are pooled, so OnEnable is more useful than Start for now
    void OnEnable()
    {
        AcquireTarget();
    }

    // Update is called once per frame
    void Update()
    {
        AcquireTarget();
        Movement();
    }

    public void AcquireTarget()
    {
        if(currentTarget == null || !currentTarget.activeInHierarchy)
        {
        Collider2D[] search = Physics2D.OverlapCircleAll(this.transform.position, fieldOfVision);
            for (int i = 0; i < search.Length; i++)
            {
                //this if statement should be altered to go to the closest of said object
                //further, where it says plant, it will be a variable based on the objects' needs
                if (search[i] != null && search[i].GetComponent<Plant>() != null && search[i] != spawnCheck)
                {
                    currentTarget = search[i].gameObject;
                }
            }
        }
        //iterate through list of colliders
        //check for whatever pressing need is
    }

    void Movement()
    {
        this.transform.position = Vector2.Lerp(this.transform.position, currentTarget.transform.position, speed * Time.deltaTime);
    }
}
