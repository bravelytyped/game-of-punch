using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spawner : MonoBehaviour
{
    //this class spawns things
    public static Spawner sharedInstance;
    //stand-in field for play area
    public float xMin, xMax, yMin, yMax;

    [SerializeField] List<ObjectPool> Pools;

    private void Awake()
    {
        sharedInstance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        InitialSpawn();
    }

    void InitialSpawn()
    {
        foreach (ObjectPool pool in Pools)
        {
            for (int i = 0; i < pool.amountToPool; i++)
            {
                Vector2 location = RandomizeLocation();
                while(!IsSpawnDestinationEmpty(location, pool.objectToPool.GetComponent<Collider2D>().bounds.size))
                {
                    location = RandomizeLocation();
                }
                SpawnOrganism(pool, location);
            }            
        }
    }

    void SpawnOrganism(ObjectPool pool, Vector2 location)
    {
        GameObject obj = pool.GetPooledObject();
        if (obj != null)
        {
            obj.transform.position = location;
            obj.SetActive(true);
        }
    }

    bool IsSpawnDestinationEmpty(Vector2 location, Vector2 objectColliderSize)
    {
        if (Physics2D.OverlapBox(location, objectColliderSize, 0f))
        {
            return false;
        }
        else if(objectColliderSize == null)
        {
            Debug.LogError("fuck, spawnlocationcheck issue over here");
            return false;
        }
        else
        {
            return true;
        }
    }

    Vector2 RandomizeLocation()
    {
        Vector2 location = new Vector2(Random.Range(xMin, xMax), Random.Range(yMin, yMax));
        return location;
    }

    public void SpawnRequest(ObjectPool pool, Vector2 spawnedObjSize, Vector2 parentPosition, int numToSpawn)
    {
        //calls the PoissonDiscSampling script to generate spawn points around an object
        List<Vector2> potentialPoints = PoissonDiscSampling.GeneratePoints(spawnedObjSize.x, new Vector2 (spawnedObjSize.x *2f, spawnedObjSize.y * 2f), 10);
        int numberSpawned = 0;
        if (potentialPoints != null)
        {
            for (int i = 0; i < potentialPoints.Count - 1; i++)
            {
                if (IsSpawnDestinationEmpty(potentialPoints[i], spawnedObjSize) && numberSpawned < numToSpawn)
                {
                    SpawnOrganism(pool, potentialPoints[i] + parentPosition);
                    numberSpawned++;
                }
            }
        }
        else
        {
            return;
        }
    }
}
