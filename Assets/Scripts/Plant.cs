using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Plant : Organism
{
    //arbitrary time before proliferate
    [SerializeField] private float timeToReproduce;
    [SerializeField] private float reproductionTimer = 0;

    void OnEnable()
    {
        Health = Random.Range(80, 100);
        NumOfOffspring = Random.Range(1, 3);
        timeToReproduce = 5;
    }

    // Update is called once per frame
    void Update()
    {
        if (NumOfOffspring > 0)
        {
            ReproductionTimerTick();
        }
    }

    void ReproductionTimerTick()
    {
        if (reproductionTimer <= timeToReproduce)
        {
            reproductionTimer += Time.deltaTime;
            if (reproductionTimer >= timeToReproduce)
            {
                reproductionTimer = 0;
                Reproduce(parentPool, this.GetComponent<Collider2D>().bounds.size, this.GetComponent<Collider2D>().transform.position, NumOfOffspring);
                NumOfOffspring -= 1;
            }
        }
    }
}
