using System.Collections;
using System.Collections.Generic;
using UnityEngine;


// starting to think this should be a parent script and make each type able to have it's own pool.
/*[System.Serializable]
public class ObjectPoolItem
{
    public int amountToPool;
    public GameObject objectToPool;
    public bool shouldExpand;
}*/

public class ObjectPool : MonoBehaviour
{
    public static ObjectPool SharedInstance;
    public List<GameObject> pooledObjects;
    public GameObject objectToPool;
    public int amountToPool;

    [SerializeField] private bool shouldExpand = true;

    private void Awake()
    {
        SharedInstance = this;
    }
    // Start is called before the first frame update
    void Start()
    {
        CreatePool();        
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    public void CreatePool()
    {
        pooledObjects = new List<GameObject>();
        for (int i = 0; i < amountToPool; i++)
        {
            GameObject obj = Instantiate(objectToPool);
            AssignPool(obj);
            obj.SetActive(false);
            pooledObjects.Add(obj);
        }
    }

    public GameObject GetPooledObject()
    {
        // iterates through pool list and activates the first available object, option to create new one if pool runs out
        for (int i = 0; i < pooledObjects.Count; i++)
        {
            if(!pooledObjects[i].activeInHierarchy)
            {
                return pooledObjects[i];
            }
        }
        if (shouldExpand == true)
        {
            GameObject obj = Instantiate(objectToPool);
            AssignPool(obj);
            obj.SetActive(false);
            pooledObjects.Add(obj);
            return obj;
        }
        else
        {
            return null;
        }
    }

    protected void AssignPool(GameObject obj)
    {
        //assigns the appropriate objectpool to a variable inside the new Organism instance
        Organism organism = obj.GetComponent<Organism>();
        organism.parentPool = this;
    }
}
